# Linux权限管理拓展训练

## 一、基础权限拓展练习
1. 用户基础权限为9位，每三位为一组，每组代表着谁的权限？
    ```
    第一组代表拥有者的权限
    第二组代表所属用户组当中的用户的权限
    第三组代表其它用户（即不是拥有者，也不是文件用户组当中的用户）的权限
    ```
2. 权限中的rwx-，每个字符所代表什么意思？对应的数字是什么？
    ```
    r代表读，数字4
    w代表写或修改，数字2
    x代表执行，数字1
    -代表无权限，数字0
    ```
3. -rwxr-xr-x，写出对应数字权限
    ```
    755
    ```
4. -rwxr--r--，写出对应数字权限
    ```
    744
    ```
5. -r-xr-x--x，写出对应数字权限
    ```
    551
    ```
6. -rw-r-xr-x，写出对应数字权限
    ```
    655
    ```
7. -r--r--r--
    ```
    444
    ```
8. -r-xr-----，写出对应数字权限
    ```
    540
    ```
9. ---x-w-r--，写出对应数字权限
    ```
    124
    ```
10. -rwxr--rw-，写出对应数字权限
    ```
    746
    ```
11. -rw-r--r--，写出对应数字权限
    ```
    644
    ```
12. ---xr--rwx，写出对应数字权限
    ```
    147
    ```
13. 777，写出对应字母权限
    ```
    rwxrwxrwx
    ```
14. 545，写出对应字母权限
    ```
    r-xr--r-x
    ```
15. 744，写出对应字母权限
    ```
    rwxr--r--
    ```
16. 600，写出对应字母权限
    ```
    rw-------
    ```
17. 641，写出对应字母权限
    ```
    rw-r----x
    ```
18. 711，写出对应字母权限
    ```
    rwx--x--x
    ```
19. 700，写出对应字母权限
    ```
    rwx------
    ```
20. 555，写出对应字母权限
    ```
    r-xr-xr-x
    ```
21. 733，写出对应字母权限
    ```
    rwx-wx-wx
    ```
22. 713，写出对应字母权限
    ```
    rwx--x-wx
    ```
23. 建一个目录/test,查看这个目录的默认权限是?
    ```
    mkdir /test
    ls -al /test
    默认权限是755
    ```

24. 进入/test目录中,建一个文件abc,查看其默认的权限为？
    ```
    cd /test
    touch abc
    ls -l
    644
    ```

25. 创建一个文件test.txt,并其将权限改为600.
    ```
    touch test.txt
    chmod 600 test.txt
    ```

26. 将test.txt文件的权限改为755.
    ```
    chmod 755 test.txt
    ```

27. 将test.txt文件的权限改为000.
    ```
    chmod 000 test.txt
    ```

28. 修改test.txt文件的权限为644.
    ```
    chmod 644 test.txt
    ```

29. 给test.txt文件的属主加上x权限。
    ```
    chmod u+x test.txt
    ```

30. 给test.txt文件的其他用户加上x权限。
    ```
    chmod o+x test.txt
    ```

31. 去除test.txt文件的所有执行权限。
    ```
    chmod a-x test.txt
    ```

32. 给/test目录及目录下的所有文件或目录的权限统一改为744。
    ```
    chmod -R 744 /test
    ```

33. 给/test目录及目录下的所有文件或目录加上x权限（属主、属组、其他用户都执行权限）。
    ```
    chmod -R a+x /test
    ```

34. 使用stat命令查看test.txt文件的权限，并使用awk将权限取出来，比如：755

35. 使用stat命令查看test.txt文件的权限，并使用sed将权限取出来，比如：755


## 二、特殊权限拓展练习

1. 创建目录/web01，所有者是user01，所属组是user02；
    ```
    mkdir  /web01
    useradd user01
    groupadd user02	创建组
    chown user01.user02 /web01
    ```

2. 复制/etc/fstab文件到/opt下，设置文件所有者为tomcat读写权限，所属组为apps组有读写权限，其他人无权限
    ```
    [root@oldboy ~]# cp /etc/fstab /opt/fstab
    [root@oldboy ~]# groupadd apps
    [root@oldboy ~]# useradd tomcat
    [root@oldboy ~]# chown tomcat.apps /opt/fstab
    [root@oldboy ~]# chmod g+w,o-r /opt/fstab
    [root@oldboy ~]# ll /opt
    total 4
    -rw-rw----. 1 tomcat apps 501 Jul  2 15:12 fstab
    ```
3. 将以下权限翻译成数字，将数字权限用字母表示
    ```
    rw-r-xr--
    654
    rw-r--r--
    644
    rwx--x--x
    711
    rw-------
    600
    rwxr--r--
    744
    rw-rw-r--
    664
    rwxrwxrwx
    777
    751			
    rwxr-x--x
    771			
    rwxrwx--x
    632		
    rw--wx-w-
    551			
    r-xr-x--x
    622			
    rw--w--w-
    746		
    rwx-w-rw-
    644			
    rw--r--r-
    755	
    rwxr-xr-x
    ```
4. 假设公司研发部的用户David和Peter属于组A，财务部的用户life和laowang属于组B，根据要求创建用户及组。
    ```
    [root@oldboy ~]# groupadd A
    [root@oldboy ~]# groupadd B
    [root@oldboy ~]# useradd David -g A
    [root@oldboy ~]# useradd Peter -g A
    [root@oldboy ~]# useradd life -g B
    [root@oldboy ~]# useradd laowang -g B
    ```
5. 接第4题，建立目录file1，该目录里面的文件只能由研发部人员读取、增加、删除、修改以及执行，其他用户不能对该目录进行任何操作
    ```
    [root@oldboy ~]# mkdir file1
    [root@oldboy ~]# ll -d file1
    drwxr-xr-x. 2 root root 6 Jul  2 15:44 file1
    [root@oldboy ~]# chown .A file1
    [root@oldboy ~]# ll -d file1
    drwxr-xr-x. 2 root A 6 Jul  2 15:44 file1
    [root@oldboy ~]# chmod 770 file1
    [root@oldboy ~]# ll -d file1
    drwxrwx---. 2 root A 6 Jul  2 15:44 file1
    ```
6. 接第题，建立目录file2，该目录里面的文件只能由财务部人员读取、增加、删除、修改以及执行，其他用户不能对该目录进行任何操作
    ```
    [root@oldboy ~]# mkdir file2
    [root@oldboy ~]# chown .B file2
    [root@oldboy ~]# chmod 770 file2
    [root@oldboy ~]# ll -d file2
    drwxrwx---. 2 root B 6 Jul  2 15:49 file2
    ```
7. 接第4题，建立目录file3,该目录里面的文件研发部人员可以读取、增加、删除、修改以及执行,其他部门只能做查看操作
    ```
    [root@oldboy ~]# mkdir file3
    [root@oldboy ~]# chown .A file3
    [root@oldboy ~]# chmod 775 file3
    [root@oldboy ~]# ll -d file3
    drwxrwxr-x. 2 root A 6 Jul  2 15:56 file3
    ```
8. 接第4题，建立目录file4，该目录里面的文件只有研发部的经理David拥有所有操作权限，研发部的其他人只有查看权限，其他部门不能进行任何操作
    ```
    [root@oldboy ~]# mkdir file4
    [root@oldboy ~]# chown David.A file4
    [root@oldboy ~]# chmod 750 file4
    [root@oldboy ~]# ll -d file4
    drwxr-x---. 2 David A 6 Jul  2 15:59 file4
    ```
9. 新建用户组，shengchan,demo,要求如下:
    + tom是shengchan组的附加成员
    + leo是demo组的附加成员
    + 新建用户admin不属于以上任何一个组
    ```
    [root@oldboy ~]# groupadd  shengchan
    [root@oldboy ~]# useradd tom -G shengchan

    [root@oldboy ~]# groupadd demo
    [root@oldboy ~]# useradd leo -G demo

    [root@oldboy ~]# useradd admin
    [root@oldboy ~]# id admin
    uid=1137(admin) gid=1137(admin) groups=1137(admin)
    ```
10. 新建目录要求如下：
    + /pub/目录为公共存储目录，所有用户可以读写执行
    + /sc目录是生产部的目录，生产部的成员读写执行，并且生产部人员建立的文件自动归属到shengchan组中
    + admin用户可以在/sc目录中可以读
    ```
    [root@oldboy ~]# mkdir /pub
    [root@oldboy ~]# chmod 777 /pub
    [root@oldboy ~]# ll -d /pub
    drwxrwxrwx. 2 root root 6 Jul  2 16:28 /pub
    [root@oldboy ~]# mkdir /sc
    [root@oldboy ~]# chown -R  .shengchan /sc
    [root@oldboy ~]# chmod  2775 /sc
    chmod 775 /sc
    ```
11. 新建目录/www01，/www02，/www03
    ```
    [root@oldboy ~]# mkdir /www{01..03}
    ```
12. 接第11题,更改/www01目录的权限,使其他用户对它没有任何权限;
    ```
    [root@oldboy ~]# chmod 750 www01
    [root@oldboy ~]# ll -d www01
    drwxr-x---. 2 root root 6 Jul  2 16:40 www01
    ```
13. 接第11题,更改/www02目录的权限,使所属组对它拥有读写执行权限;
    ```
    [root@oldboy ~]# chmod 770 www02
    ```
14. 接第11题,更改/www03目录的权限,任何用户都可以读写,但是在/www03目录中创建的任何文件都属于grp1组
    ```
    [root@oldboy ~]# groupadd grp1
    [root@oldboy ~]# chown .grp1 /www03
    [root@oldboy ~]# chmod 2777 /www03
    [root@oldboy ~]# ll -d /www03
    drwxrwsrwx. 2 root grp1 6 Jul  2 16:40 /www03
    ```
15. 新建用户zhangsan，lisi，wangergou，三个用户都属于同一个用户组f4,密码都为oldboy
    ```
    [root@oldboy ~]# groupadd f4
    [root@oldboy ~]# useradd zhangshan -g f4
    [root@oldboy ~]# useradd lisi -g f4
    [root@oldboy ~]# useradd wangergou -g f4
    [root@oldboy ~]# echo oldboy|passwd --stdin zhangshan
    Changing password for user zhangshan.
    passwd: all authentication tokens updated successfully.
    [root@oldboy ~]# echo oldboy|passwd --stdin lisi
    Changing password for user lisi.
    passwd: all authentication tokens updated successfully.
    [root@oldboy ~]# echo oldboy|passwd --stdin wangergou
    Changing password for user wangergou.
    passwd: all authentication tokens updated successfully.
    ```
16. 接15题，上述用户和组都能在/data/code目录，访问，创建，删除文件，其他用户无法访问该目录
    ```
    [root@oldboy ~]# mkdir /data/code
    [root@oldboy ~]# chown .f4 /data/code
    [root@oldboy ~]# chmod 770 /data/code
    [root@oldboy ~]# ll -d /data/code
    drwxrwx---. 2 root f4 6 Jul  2 16:59 /data/code
    ```
17. 接15题，/data/code目录下创建的所有文件自动归属于f4组所有
    ```
    [root@oldboy ~]# chmod g+s /data/code
    [root@oldboy ~]# ll -d /data/code
    drwxrws---. 2 root f4 6 Jul  2 16:59 /data/code
    ```
18. 接15题，现在新增了一批用户，属于默认组，需要开放其他用户在code目录的读权限
    ```
    [root@oldboy ~]# chmod o+rx /data/code
    [root@oldboy ~]# ll -d /data/code
    drwxrwsr-x. 2 root f4 6 Jul  2 16:59 /data/code
    ```
19. 接15题，新增的所有其他用户在code目录下创建的文件自动归属f4组
    ```
    [root@oldboy ~]# chmod 777 /data/code
    ```
20. 有两个用户组，分别为python组、linux组，python组的人可以修改读取python组的文件，但不能让linux组的人读取；linux组的人可以修改读取linux组的文件，但不能让python组的人读取。
    ```
    [root@oldboy ~]# groupadd python
    [root@oldboy ~]# touch python
    [root@oldboy ~]# chmod g=rwx,o=- python
    [root@oldboy ~]# chown .python python


    [root@oldboy ~]# groupadd linux
    [root@oldboy ~]# touch linux
    [root@oldboy ~]# chown .linux linux
    [root@oldboy ~]# chmod g=rwx,o=- linux
    ```
21. 在当前目录中新建文件text，假设该文件的权限为614。现要求设置该文件属主(u)增加执行权限，属组(g)增加写权限，其他用户(o)删除读权限，应该如何操作，另外修改后的权限用字母应该如何表示
    ```
    [root@oldboy ~]# chmod u+x,g+w,o-r text
    -rwx-wx---
    ```
22. 在当前目录中创建目录aaa，并把该目录的权限设置为只有文件属主有读、写和执行权限
    ```
    [root@oldboy ~]# mkdir aaa
    [root@oldboy ~]# chmod 700 aaa
    [root@oldboy ~]# ll -d aaa
    drwx------. 2 root root 6 Jul  2 19:14 aaa
    ```
23. 设某文件myfile的权限为-rw-r--r--，若要增加所有人可执行的权限,应该怎么做
    ```
    chmod +x myfile
    ```
24. 登录到普通用户user01，删除/opt这个目录，提示权限不足，如果要删除/opt这个目录，不使用root用户删除的话，你该怎么解决？
    ```
    chmod u+s /usr/bin/rm
    ```
25. 创建目录/test，要求所有用户对这个目录有所有权限，现在只想让每个用户在这个目录只能对属于自己的文件进行操作，怎么实现？
    ```
    mkdir /test
    chmod 777 /test
    chmod o+t /test
    ```
26. 创建一个文件/opt/file.txt,这个文件很重要,现要求所有人只能查看和追加内容进去,不允许有其他的操作,怎么实现?
    ```
    touch /opt/file.txt
    chmod o=rw /opt/file.txt
    chattr +a /opt/file.txt
    ```
27. 取消上题的设置，现要求所有人只能查看此文件，不允许有其他的操作，怎么实现？
    ```
    chattr -a /opt/file.txt
    chattr +i /opt/file.txt
    ```
28. 请问在当前系统中新创建的目录和文件，权限各是多少，为什么？
    ```
    目录：755,0777-0022（umask）
    文件：644,0666-0022
    ```
29. 如果我想要在系统中新创建的目录权限为555，文件为444，怎么实现？
    ```
    umask 222
    ```
30. umask怎么计算目录及文件权限的？
    ```
    目录：777-umask
    文件：666-umask,若umask有奇数位，则对应的权限为数字+1
    ```
31. 输入last，统计当前连接本机的每个远程终端IP的连接总次数
    ```
    [root@oldboy ~]# last|awk '/pts/ {print $3}'|uniq -c
    38 10.0.0.1
    32. 输入lscpu，取出当前系统的cpu核心数
    [root@oldboy ~]# lscpu|awk -F '[: ]+' 'NR==7{print $4}'
    1
    ```
33. 输入df -h，取出当前系统根分区剩余可用磁盘空间
    ```
    [root@oldboy ~]# df -h|awk -F ' ' 'NR==2{print $4}'

    18G 
    ```
34. 显示/proc/meminfo文件中以s开头的行(忽略大小写)
    ```
    [root@oldboy ~]# grep -i '^s' /proc/meminfo
    SwapCached:            0 kB
    SwapTotal:       1048572 kB
    SwapFree:        1048572 kB
    Shmem:             14048 kB
    Slab:              94176 kB
    SReclaimable:      46988 kB
    SUnreclaim:        47188 kB
    ```
35. 扩展题：在你修改了test.txt文件之后，如何知道在你下次要修改这个文件时，这个文件有没有被别人修改过?
    ```
    [root@centos7 ~]# touch test.txt
    [root@centos7 ~]# echo 123 > test.txt
    [root@centos7 ~]# md5sum test.txt > figer.txt
    [root@centos7 ~]# md5sum -c figer.txt
    test.txt: OK
    ```


## 三、默认权限umask

    umask 用于设置默认去掉的权限，通常为0022，第一位为特殊权限位，后面的三位表示：给同一组的用户和其它用户都去掉写权限

    而文件夹的默认权限是：777 -022 755 rwxr-xr-x
    而文件的默认权限是：666 - 022 644  rw-r--r--